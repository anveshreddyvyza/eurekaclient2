package com.example.EurekaClient2;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/student")
public class Controller {
	
    @Autowired

    private RestTemplate restTemplate;
	@SuppressWarnings("unchecked")
	@GetMapping(value="/list/{name}",produces = MediaType.APPLICATION_JSON_VALUE)
	public Student[] getAllStudents(@PathVariable String name){
		System.out.println("School name is "+name);
	
		
		Student[] listAll= restTemplate.getForObject("http://desktop-lo9442k:8083/eureka-client/users/user/vyza", Student[].class);
		return listAll;
		
	}
}
